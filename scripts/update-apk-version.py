#/usr/bin/python

import sys
import os
import re
import argparse

__author__ = 'Francisco Sierra'
__maintainer__ = "Francisco Sierra"
__version__ = "0.1"
__status__ = "SNAPSHOT"

def readFile(fileName):
    file_str = str()
    with open(fileName, "r") as file:
        file_str = file.read()
    return file_str

def writeFile(filePath, content):
    with open(filePath, "w") as file:
        file.write(content)

def closeProgram(failMsg):
    echo("SEVERE: " + failMsg)
    echo("Cerrando programa")
    sys.exit(1)

def updateVersions(args):
    applyFunctionToFiles(args.directory, updateVersionToFile, args.tag)

def applyFunctionToFiles(path, function, tag):
    projectDirs = ["app"]
    if not os.path.exists(path):
        return False
    for projectDir in projectDirs:
        newDir = os.path.join(os.path.abspath(path), projectDir)
        if not os.path.exists(newDir):
            continue
        for dir in os.listdir(newDir):
            new_path = os.path.join(os.path.abspath(newDir), dir)
            if os.path.isdir(new_path):
                applyFunctionToFiles(new_path, function, tag)
            function(new_path, tag)

def updateVersionToFile(filePath, tag):
    if re.match(r'.*build\.gradle$', filePath) :
        echo("Cambiando a version {0} el archivo : {1}".format(tag, filePath))
        fileContent = readFile(filePath)
        fileContent = parseGradle(fileContent, tag)
        writeFile(filePath, fileContent)

def parseGradle(fileContent, tag, propertyParent="defaultConfig", property="versionName", code="versionCode"):
    config = False
    versionChange = False
    versionCode = False
    for line in fileContent.split("\n"):
        if config and property in line:
            newLine = line.replace(getLastVersion(line), tag)
            echo(newLine)
            fileContent = fileContent.replace(line, newLine)
            versionChange = True
        if config and code in line:
            currentCode = getLastVersionCode(line)
            newCode = currentCode + 1
            newLine = line.replace(str(currentCode), str(newCode))
            fileContent = fileContent.replace(line, newLine)
            versionCode = True
        if propertyParent in line:
            config = True
        if versionChange and versionCode:
            break
    return fileContent

def getLastVersion(line):
    return line.split("\"")[1]

def getLastVersionCode(line):
    return int(line.strip().split(" ")[1])
   
def echo(string):
    os.system("echo \"{0}\"".format(string))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='This is a git-flow automation script .')
    parser.add_argument('-d','--directory', help='Project path',required=True)
    parser.add_argument('-t','--tag', help='Project path',required=True)

    args = parser.parse_args()

    echo(args.directory)
    os.chdir(args.directory)
    # args.directory = os.path.dirname(os.path.realpath(__file__))

    updateVersions(args)
    sys.exit()
