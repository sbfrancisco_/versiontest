import os

version_list = """git tag -l '1.*' --sort="-version:refname" """
lista = os.popen(version_list).read().splitlines()

compare = ""
for index, version in enumerate(os.popen(version_list).read().splitlines()):
  if index == 0:
    compare = version
    continue
  changes_list = os.popen("git log --oneline {0}..{1} | cut -d' ' -f2- | uniq | nl -b t ".format(version, compare)).read()
  timestamp = os.popen("git log -1 {0} --format=%ai ".format(compare)).read().strip()
  changeLog = """

---

##{0}
{1}

{2}

""".format(compare, timestamp, changes_list).strip()
  print(changeLog)
  compare=version
