package com.example.versiontest.ui.main;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.example.versiontest.util.ReactiveEvent;

public class MainViewModel extends ViewModel {

    ReactiveEvent<String> versionName;
    ReactiveEvent<String> versionCode;

    public MainViewModel() {
        versionName = new ReactiveEvent<>();
        versionCode = new ReactiveEvent<>();
    }

    void showName(VersionApp versionApp) {
        versionName.postValue(versionApp.getVersionName());
    }

    void showCode(VersionApp versionApp) {
        versionCode.postValue(String.valueOf(versionApp.getVersionCode()));
    }

}
