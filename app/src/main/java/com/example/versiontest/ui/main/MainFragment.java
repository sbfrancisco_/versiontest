package com.example.versiontest.ui.main;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.versiontest.BuildConfig;
import com.example.versiontest.R;

public class MainFragment extends Fragment {

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    private MainViewModel mViewModel;

    private View rootView;

    private TextView versionName;
    private TextView versionCode;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.main_fragment, container, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        versionName = rootView.findViewById(R.id.message);
        versionCode = rootView.findViewById(R.id.message_code);

        observeStreams();

        VersionApp versionApp = new VersionApp(BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE);
        mViewModel.showName(versionApp);
        mViewModel.showCode(versionApp);

    }

    private void observeStreams() {
        mViewModel.versionName.observe(this, new Observer() {
            @Override
            public void onChanged(Object o) {
                versionName.setText((String) o);
            }
        });

        mViewModel.versionCode.observe(this, new Observer() {
            @Override
            public void onChanged(Object o) {
                versionCode.setText((String) o);
            }
        });
    }

}
